# This Python file uses the following encoding: utf-8
import sys
from PyQt5.QtWidgets import QApplication
from widget import Widget

if __name__ == "__main__":
    app = QApplication([])
    window = Widget()
    window.show()
    sys.exit(app.exec_())
