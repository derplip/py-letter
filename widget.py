#!/usr/bin/env python3
import sys
from PyQt5 import QtCore, QtGui
from PyQt5.QtWidgets import QWidget, QApplication, QFileDialog
from UI_widget import Ui_Widget
from letter import Letter


class Widget(QWidget):

    def __init__(self, parent=None):
        QWidget.__init__(self, parent)
        self.ui = Ui_Widget()
        self.ui.setupUi(self)

        self.ui.pushButtonGenerate.clicked.connect(self.generateLetter)

    def getSenderAddress(self):

        ret = ""
        if self.ui.lineEditSenderName2.text():
            ret += self.ui.lineEditSenderName2.text() + ", " 
        ret += self.ui.lineEditSenderStreet.text() + ", " 
        if self.ui.lineEditSenderAddition.text():
            ret += self.ui.lineEditSenderAddition.text() + ", " 
        ret += self.ui.lineEditSenderCity.text()
        return ret

    def getRecipantAddress(self):

        ret = ""
        if self.ui.lineEditRecipantName2.text():
            ret += self.ui.lineEditRecipantName2.text() + r" \\ " 
        ret += self.ui.lineEditRecipantStreet.text() + r" \\ " 
        if self.ui.lineEditRecipantAddition.text():
            ret += self.ui.lineEditRecipantAddition.text() + r" \\ " 
        ret += self.ui.lineEditRecipantCity.text()
        return ret

    def generateLetter(self):

        letter = Letter()
        letter.setFromName(self.ui.lineEditSenderName.text())
        letter.setFromAddress(self.getSenderAddress())

        letter.setToName(self.ui.lineEditRecipantName.text())
        letter.setToAddress(self.getRecipantAddress())

        letter.setSubject(self.ui.lineEditSubject.text())
        letter.setOpening(self.ui.lineEditOpening.text())
        letter.setText(self.ui.plainTextEditLetterText.toPlainText())
        letter.setClosing(self.ui.lineEditClosing.text())
        

        letter.generate()


if __name__ == "__main__":
    app = QApplication(sys.argv)
    w = Widget()
    w.show()
    sys.exit(app.exec_())
