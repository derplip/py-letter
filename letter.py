import sys, os
from pylatex import Document, NoEscape, Command


class Letter:
    def __init__(self):

        self._doc = Document(documentclass='scrlttr2',
                             document_options='12pt,ngerman',
                             lmodern=False,
                             textcomp=False,
                             page_numbers = False)
                            
        self._doc.preamble.append(
                    Command('usepackage', 'babel', 'ngerman'))

        self._komavars = {}
        self._opening = ""
        self._closing = ""
        self._text= ""

    def setFromName(self, name):
        self._komavars['fromname'] = name

    def setFromAddress(self, addr):
        self._komavars['fromaddress'] = addr

    def setToName(self, name):
        self._komavars['toname'] = name

    def setToAddress(self, addr):
        self._komavars['toaddress'] = addr

    def setSubject(self, sub):
        self._komavars['subject'] = sub

    def setOpening(self, text):
        self._opening = text

    def setClosing(self, text):
        self._closing = text

    def setText(self, text):
        self._text = text

    def generate(self, outpath="./letter", genTex=True, genPdf=True):

        for var in self._komavars:
            if self._komavars[var]:
                self._doc.preamble.append(
                    Command('setkomavar',
                            [var, NoEscape(self._komavars[var])]))

        self._doc.append(NoEscape(r'\begin{letter}{}'))
        self._doc.append(NoEscape(r'\opening{' + self._opening + r'}'))
        self._doc.append(self._text)
        self._doc.append(NoEscape(r'\closing{' + self._closing + r'}'))
        self._doc.append(NoEscape(r'\end{letter}'))       
        
        if genPdf:
            self._doc.generate_pdf(outpath)
        if genTex:
            self._doc.generate_tex(outpath)
        


if __name__ == '__main__':

    letter = Letter()
    letter.setFromName("Peter Pan")
    letter.setFromAddress("Verlorene Straße. 1, 99999 Nimmerland")
    letter.setToName("Max Mustermann")
    letter.setToAddress(r"Musterstraße 1 \\ 12345 Musterstadt")
    letter.setOpening("Sehr geehrter Herr Mustermann")
    letter.setClosing("Viele Grüße")
    letter.setText("Was soll ich bloß schreiben?\nIch weiß es einfach nicht...\nEgal.")
    letter.generate()